# Nightly Builds of tcpdump

This repository only contains a CI/CD pipeline to build and deploy nightly builds of tcpdump for amd64, arm64 and arm.

Capture packages example
```
docker run --net=host procinger/tcpdump:latest -i any
```